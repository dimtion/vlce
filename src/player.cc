#include "player.h"
#include <iostream>

namespace vlce {

using v8::Local;
using v8::Object;
using v8::Isolate;
using v8::FunctionTemplate;
using v8::Persistent;
using v8::Function;
using v8::Context;
using v8::Value;

Persistent<Function> Player::constructor;


Player::Player() {
  char const *argv[] = {
    "--intf",
    "torrent",
  };
  int argc = 2;
  vlcInstance = VLC::Instance(argc, argv);
  vlcInstance.addIntf("torrent");
  vlcMediaPlayer = VLC::MediaPlayer(vlcInstance);

  vlcMediaPlayer.setVideoCallbacks(
    [this](void** pBuffer) -> void* {
      *pBuffer = this->imageData;
      return nullptr;
    },
    [this](void* pictureId, void*const* planes) -> void {
    },
    [this](void* pictureId) -> void {
      this->event = 0;
      uv_async_send(&async);
    }
  );

  vlcMediaPlayer.setVideoFormatCallbacks([this](char* chroma, uint32_t* width, uint32_t* height, uint32_t* pitch, uint32_t* lines) -> int {
      memcpy(chroma, "RV32", 4);
      *width = this->vlcMedia.tracks()[0].width();
      *height = this->vlcMedia.tracks()[0].height();
      *pitch = *width * 4;
      *lines = *height;


      this->width = *width;
      this->height = *height;

      // create default imageBuffer
      // TODO: This is not thread safe, we should find another solution later
      this->event = 1;
      uv_async_send(&async);

      return 1;
      }, []() -> void {
  });

  loop = uv_default_loop();
  async.data = this;
  uv_async_init(loop, &async, [](uv_async_t* handle) {
      (reinterpret_cast<Player*>(handle->data))->VLCEventReceived();
  });
}

Player::~Player() {
  uv_close((uv_handle_t*) &async, NULL);
}

void Player::updateImageBuffer() {
  Isolate* isolate = v8::Isolate::GetCurrent();
  size_t bufferSize = width * height * 4;

  auto localImgBuffer = v8::ArrayBuffer::New(isolate, bufferSize);
  auto localImg = v8::Uint8Array::New(localImgBuffer, 0, bufferSize);

  this->imageBuffer.Reset(isolate, localImgBuffer);
  this->image.Reset(isolate, localImg);
  this->imageData = (char*) node::Buffer::Data((v8::Local<v8::Object>) localImg);
}

void Player::VLCEventReceived() {
  switch (this->event) {
    case 0:
      draw();
      break;
    case 1:
      updateImageBuffer();
      break;

  }
}

void Player::draw() {
  v8::Isolate* isolate = v8::Isolate::GetCurrent();
  const unsigned argc = 1;

  Local<v8::Uint8Array> img = v8::Local<v8::Uint8Array>::New(isolate, this->image);
  Local<Value> argv[argc] = { img };
  Local<Function> cb = v8::Local<Function>::New(isolate, this->drawCb);
  cb->Call(Null(isolate), argc, argv);
}

void Player::Init(Local<Object> exports) {
  Isolate* isolate = exports->GetIsolate();

  // Create function
  Local<FunctionTemplate> tpl = FunctionTemplate::New(isolate, NewInstance);
  tpl->SetClassName(v8::String::NewFromUtf8(isolate, "Player"));
  tpl->InstanceTemplate()->SetInternalFieldCount(1);  // TODO: verify this

  NODE_SET_PROTOTYPE_METHOD(tpl, "play", Play);
  NODE_SET_PROTOTYPE_METHOD(tpl, "pause", Pause);
  NODE_SET_PROTOTYPE_METHOD(tpl, "stop", Stop);
  NODE_SET_PROTOTYPE_METHOD(tpl, "isPlaying", IsPlaying);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setMedia", SetMedia);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setTime", SetTime);
  NODE_SET_PROTOTYPE_METHOD(tpl, "getTime", GetTime);
  NODE_SET_PROTOTYPE_METHOD(tpl, "setVolume", SetVolume);
  NODE_SET_PROTOTYPE_METHOD(tpl, "getVolume", GetVolume);
  NODE_SET_PROTOTYPE_METHOD(tpl, "getDuration", GetDuration);
  // NODE_SET_PROTOTYPE_METHOD(tpl, "setOptions", SetOptions);
  NODE_SET_PROTOTYPE_METHOD(tpl, "_getWidth", GetWidth);
  NODE_SET_PROTOTYPE_METHOD(tpl, "_getHeight", GetHeight);
  NODE_SET_PROTOTYPE_METHOD(tpl, "_setCallback", SetJSCallback);
  constructor.Reset(isolate, tpl->GetFunction());
}

void Player::Play(const v8::FunctionCallbackInfo<v8::Value>& args) {
  if (args.Length() > 1) {
    SetMedia(args);
  }
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());
  obj->vlcMediaPlayer.play();
}

void Player::IsPlaying(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  auto playing = v8::Boolean::New(isolate, obj->vlcMediaPlayer.isPlaying());
  args.GetReturnValue().Set(playing);
}

void Player::Pause(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());
  obj->vlcMediaPlayer.pause();
}

void Player::SetVolume(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();

  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  if (args.Length() < 1) {
    isolate->ThrowException(v8::Exception::TypeError(
          v8::String::NewFromUtf8(isolate, "No arguments given")));
    return;
  }

  int volume = args[0]->NumberValue();
  obj->vlcMediaPlayer.setVolume(volume);
}

void Player::GetVolume(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  auto volume = v8::Number::New(isolate, obj->vlcMediaPlayer.volume());
  args.GetReturnValue().Set(volume);
}

void Player::SetMute(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();

  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  if (args.Length() < 1) {
    isolate->ThrowException(v8::Exception::TypeError(
          v8::String::NewFromUtf8(isolate, "No arguments given")));
    return;
  }

  bool mute = args[0]->BooleanValue();
  obj->vlcMediaPlayer.setVolume(mute);
}

void Player::GetMute(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  auto mute = v8::Number::New(isolate, obj->vlcMediaPlayer.mute());
  args.GetReturnValue().Set(mute);
}

void Player::ToggleMute(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());
  obj->vlcMediaPlayer.toggleMute();
}

void Player::SetTime(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();

  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  if (args.Length() < 1) {
    isolate->ThrowException(v8::Exception::TypeError(
          v8::String::NewFromUtf8(isolate, "No arguments given")));
    return;
  }

  int64_t time = args[0]->NumberValue();
  obj->vlcMediaPlayer.setTime(time);
}

void Player::GetTime(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  auto time = v8::Number::New(isolate, obj->vlcMediaPlayer.time());
  args.GetReturnValue().Set(time);
}

void Player::GetDuration(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  auto duration = v8::Number::New(isolate, obj->vlcMedia.duration());
  args.GetReturnValue().Set(duration);
}

void Player::SetOptions(const v8::FunctionCallbackInfo<v8::Value>& args) {

  Isolate* isolate = args.GetIsolate();
  // Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  if (args.Length() < 1) {
    isolate->ThrowException(v8::Exception::TypeError(
          v8::String::NewFromUtf8(isolate, "No arguments given")));
    return;
  }
}

void Player::Stop(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());
  obj->vlcMediaPlayer.stop();
}

void Player::SetJSCallback(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  if (args.Length() < 1) {
    isolate->ThrowException(v8::Exception::TypeError(
          v8::String::NewFromUtf8(isolate, "No arguments given")));
    return;
  }

  auto drawCb = Local<Function>::Cast(args[0]);

  obj->drawCb.Reset(isolate, drawCb);

}

void Player::SetMedia(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());
  if (args.Length() < 1) {
    isolate->ThrowException(v8::Exception::TypeError(
          v8::String::NewFromUtf8(isolate, "No arguments given")));
    return;
  }

  v8::String::Utf8Value mediaUrlUtf8(args[0]);
  char* mediaUrl = *mediaUrlUtf8;

  // TODO: Dont always use "FromPath"
  obj->vlcMedia = VLC::Media(obj->vlcInstance, mediaUrl, VLC::Media::FromPath);
  // obj->vlcMedia.parse();
  obj->vlcMediaPlayer.setMedia(obj->vlcMedia);
}

void Player::GetWidth(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  auto width = v8::Number::New(isolate, obj->width);
  args.GetReturnValue().Set(width);
}

void Player::GetHeight(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();
  Player* obj = ObjectWrap::Unwrap<Player>(args.Holder());

  auto height = v8::Number::New(isolate, obj->height);
  args.GetReturnValue().Set(height);
}


void Player::NewInstance(const v8::FunctionCallbackInfo<v8::Value>& args) {
  Isolate* isolate = args.GetIsolate();

  if (args.IsConstructCall()) {
    Player* obj = new Player();
    obj->Wrap(args.This());
    args.GetReturnValue().Set(args.This());
  } else {
    Local<Value> argv[0] = {};
    Local<Function> cons = Local<Function>::New(isolate, constructor);
    Local<Context> context = isolate->GetCurrentContext();
    Local<Object> instance = cons->NewInstance(context, 0, argv).ToLocalChecked();
    args.GetReturnValue().Set(instance);
  }
}

}

#ifndef VLCE_PLAYER
#define VLCE_PLAYER

#include <node.h>
#include <nan.h>
#include <node_object_wrap.h>
#include <uv.h>
#include <vlcpp/vlc.hpp>

namespace vlce {

class Player : public node::ObjectWrap {
  public:
    static void Init(v8::Local<v8::Object> exports);
    static void NewInstance(const v8::FunctionCallbackInfo<v8::Value>& args);
    Player();

  private:
    ~Player();

    static v8::Persistent<v8::Function> constructor;

    static void SetOptions(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void SetMedia(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void Play(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void Pause(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void IsPlaying(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void SetTime(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void GetTime(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void GetDuration(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void SetVolume(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void GetVolume(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void GetMute(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void SetMute(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void ToggleMute(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void SetJSCallback(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void GetWidth(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void GetHeight(const v8::FunctionCallbackInfo<v8::Value>& args);
    static void Stop(const v8::FunctionCallbackInfo<v8::Value>& args);

    void updateImageBuffer();
    void VLCEventReceived();
    void draw();

    v8::Persistent<v8::ArrayBuffer> imageBuffer;
    v8::Persistent<v8::Uint8Array> image;
    v8::Persistent<v8::Function> drawCb;
    char* imageData;
    v8::Isolate* iso;
    uv_async_t async;
    uv_loop_t*  loop;

    VLC::Instance vlcInstance;
    VLC::Media vlcMedia;
    VLC::MediaPlayer vlcMediaPlayer;

    uint32_t width;
    uint32_t height;

    int event = 0;
};

}

#endif

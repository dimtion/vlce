#include <node.h>
#include "player.h"


namespace vlce {
using v8::Local;
using v8::Object;

void InitAll(Local<Object> exports, Local<Object> module) {
  Player::Init(exports);

  NODE_SET_METHOD(exports, "_createPlayer",
    [](const v8::FunctionCallbackInfo<v8::Value>& args)-> void {
      Player::NewInstance(args);
    });
}

NODE_MODULE(vlce, InitAll)

}

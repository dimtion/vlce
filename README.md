VLCE
====

Bindings between VLC and electron


## Quickstart guide

Before installing the dependencies you'll need LibVLC installed. You may
update the following line in `binding.gyp`, to match to your LibVLC install
directory:
```
"library_dirs": [
    "/usr/local/lib",
],
```

You'll also need [node-gyp](https://github.com/nodejs/node-gyp) to be able to
build VLCE

Configure and build:
```bash
vim env.sh
yarn rebuild
```

## API

Works on WebGL surfaces


```javascript
// Import and create a new player
let VLCPlayer = require('vlce')
let player = new VLCPlayer(args)

// Attach the player to a canvas
player.attach("#video-player")

// Set the parameters
player.setOptions({
  width: 480,
  height: 720
})

player.play('https://www.youtube.com/watch?v=Z_CUiPTfZfE')

player.pause()

player.seek(5) // in seconds

player.play()

player.setVolume(.5)

player.stop()

// or
player.setMedia('https://www.youtube.com/watch?v=Z_CUiPTfZfE')
player.play()
```

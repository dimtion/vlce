{
  "targets": [
    {
      "target_name": "vlce",
      "include_dirs": [
        "lib/libvlcpp",
        "<!(node -e \"require('nan')\")",
      ],
      "sources": [
        "src/player.cc",
        "src/vlce.cc",
      ],
      "cflags!": [ "-fno-exceptions" ],
      "cflags_cc!": [ "-fno-exceptions" ],
      "link_settings": {
        "libraries": [
            "-lvlc",
        ],
        "library_dirs": [
            "/usr/local/lib",
        ],
      }
    }
  ]
}

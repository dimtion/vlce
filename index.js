const vlce = require('bindings')('vlce')
const twgl = require('twgl.js')

const v_shader = `
attribute vec2 position;
attribute vec2 texcoord;

varying vec2 v_texCoord;

void main() {
  gl_Position = vec4(position * vec2(1, -1), 0, 1);
  v_texCoord = texcoord;
}
`

const f_shader = `
precision mediump float;

uniform sampler2D u_image;

// the texCoords passed in from the vertex shader.
varying vec2 v_texCoord;

void main() {
  gl_FragColor = texture2D(u_image, v_texCoord).bgra;
}
`

vlce.createPlayer = function () {
  const player = vlce._createPlayer()

  let gl
  let programInfo
  let bufferInfo
  let texture

  const arrays = {
    position: {
      numComponents: 2,
      data: new Float32Array([
        -1.0,  -1.0,
        1.0,  -1.0,
        -1.0,  1.0,
        -1.0,  1.0,
        1.0,  -1.0,
        1.0,  1.0,
      ])
    },
    texcoord: {
      numComponents: 2,
      data: new Float32Array([
        0.0,  0.0,
        1.0,  0.0,
        0.0,  1.0,
        0.0,  1.0,
        1.0,  0.0,
        1.0,  1.0,
      ])
    }
  }

  draw = (frame) => {
    twgl.setTextureFromArray(gl, texture, frame, {width: player._getWidth(), height: player._getHeight()})

    // gl.canvas.width = player.getWidth()
    // gl.canvas.height = player.getHeight()
    // twgl.resizeCanvasToDisplaySize(gl.canvas)
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height)

    gl.useProgram(programInfo.program)
    twgl.setBuffersAndAttributes(gl, programInfo, bufferInfo)
    twgl.drawBufferInfo(gl, bufferInfo)

    //requestAnimationFrame(draw)
  }

  player.attach = (id) => {
    console.log('Attach')
    gl = twgl.getWebGLContext(document.getElementById(id))

    programInfo = twgl.createProgramInfo(gl, [v_shader, f_shader])
    bufferInfo = twgl.createBufferInfoFromArrays(gl, arrays)

    texture = twgl.createTexture(gl, {
      target: gl.TEXTURE_2D,
      minMag: gl.NEAREST,
      wrap: gl.CLAMP_TO_EDGE,
      type: gl.UNSIGNED_BYTE,
    })

    player._setCallback(draw)
  }

  player.setOptions = (options) => {
    width = options.width || player.getWidth()
    height = options.height || player.getheight()

    gl.canvas.width = width
    gl.canvas.height = height
  }

  return player
}
module.exports = vlce
